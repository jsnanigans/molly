# Molly the store manager

Lets you create listeners from anywhere and notifies you when a value in the store has changed.

![Molly](https://i.imgur.com/DcBWBPx.jpg)

## Usage
Initiate your store with some data or just a empty object
```javascript
const molly = new Molly({})
```

## Watch for changes
You can watch for changes in the store with the `watch` method. The first parameter must be a `string` which is the key in the store, the second is a `method` that runs when the data changes and when the watcher is setup initially.
```javascript
molly.watch('user_auth_token', (token) => {
  // runs whenever `user_auth_token` the data changes
})
```
you can also watch for "deeper" changes inside objects inside the store by passing a string with a `.` separator like so:
```javascript
molly.watch('user.token', (token) => {
  // runs whenever `user.token` changes
})
```

## Set value
To set a value in the store use the `set` method. The first argument must be a `string` that is the key for the store, the second must be a `method`, it gets passed one parameter that is the current value of the store, whatever is returned becomes the new value

```javascript
molly.set('user_auth_token', () => '<<mytoken>>')
```
to set a value deeper in the store use this syntax:
```javascript
molly.set('user.token', () => '<<mytoken>>')
```


## Actions
Define global functions from inside some scope that can be called from anywhere

Add a new action to the store
```javascript
molly.addAction('notify_user', (data) => {
  this.removeOldNotifications()
  this.showNotification(data)
})
```
then you can call it from anywhere else
```html
<button onclick="molly.action.notify_user('Hello user')">Say hello</button>
```
