export default class Molly {
  store: any
  action: any = {}
  listeners: any[] = []

  constructor (store: object) {
    this.store = store
  }

  addAction (key: string, method: (any: any) => any) {
    const value = this.getValue(key)
    this.action[key] = method
  }

  private addListener (key: string, method: (any: any) => any) {
    this.listeners.push({
      key,
      method
    })
    const value = this.getValue(key)
    if (typeof value !== 'undefined') {
      method(this.getValue('key'))
    }
  }

  public get actions () {
    return this.action
  }

  // private watch (key: string, method: (storeValue) => void) {
  //   this.addListener(key, method)
  // }

  watch (path: string, method: (storeValue: any) => void) {
    if (!path) {
      return
    }

    if (path.indexOf('.') === -1) {
      this.addListener(path, method)
      return
    }

    // deep
    const parts = path.split('.')
    const root = parts[0].replace(/^(!)/g, '')

    this.addListener(root, (branch) => {
      method(this.getValue(path))
    })
  }

  getValue (path: string) {
    const parts = path.split('.')
    const modifiers = {
      invert: false
    };

    if (parts[0].indexOf('!') === 0) {
      modifiers.invert = true;
      parts[0] = parts[0].replace(/^(!)/g, '')
    }

    const root = parts[0]

    let value: any = this.store
    for (const key of parts) {
      if (value instanceof Object) {
        value = value[key]
      } else {
        break
      }
    }

    value = this.modifieValue(modifiers, value)
    return value
  }

  private modifieValue(modifiers: any, value: any) {
    if (modifiers.invert) {
      value = !value
    }
    return value
  }

  set (path: string, mutation: (storeValue: any) => any) {
    if (!path) {
      return
    }

    const value = this.getValue(path)
    const newValue = mutation(value)

    if (path.indexOf('.') === -1) {
      // root change
      if (newValue !== value) {
        this.store[path] = newValue
      }
    } else {
      // deep
      const parts = path.split('.')
      const root = parts[0]
      path = root
      const target = parts[parts.length - 1]
      const rootClone = {...this.store[root]}
      let tmpValue = rootClone

      for (const key of parts) {
        if (key !== root) {
          if (tmpValue instanceof Object) {
            if (target === key) {
              tmpValue[key] = newValue
            } else {
              tmpValue = tmpValue[key]
            }
          }
        }
      }

      this.store[root] = rootClone
    }

    // notify listeners
    if (newValue !== value) {
      const l = this.listeners.length

      for (let i = 0; i < l; i++) {
        if (this.listeners[i] && this.listeners[i].key === path) {
          this.listeners[i].method(newValue)
        }
      }
    }
  }

}